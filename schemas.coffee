exports.provider =
  "blackouts":
    "type": "array"
  "bookinglogo":
    "type": "string"
  "businesshours":
    "type": "object"
  "email":
    "type": "string"
  "password":
    "type": "string"
  "name":
    "type": "string"
  "firstname":
    "type": "string"
  "lastname":
    "type": "string"
  "orderofstages":
    "type": "array"
    "items": "string"
  "questions":
    "type": "array"
  "locations":
    "type": "array"
  "regions":
    "type": "array"
  "reservationtypes":
    "type": "array"
  "resources":
    "type": "array"
  "subdomain":
    "type": "string"
  "required": ["subdomain", "name", "email", "firstname", "lastname", "password"]

exports.blackout =
  "end":
    "type": "string"
  "provider":
    "type": "string"
  "name":
    "type": "string"
  "start":
    "type": "string"
  "targettype":
    "type": "string"
  "targetid":
    "type": "string"
  "required": ["start", "end", "targetid", "targettype", "provider"]

exports.bookable =
  "availability":
    "type": "object"
  "subcategories":
    "type": "array"
  "duration":
    "type": "integer"
  "name":
    "type": "string"
  "notifications":
    "type": "array"
  "price":
    "type": "integer"
  "provider":
    "type": "string"
  "questions":
    "type": "array"
  "requiredresources":
    "type": "array"
  "seats":
    "type": "integer"
  "seatsperbooking":
    "type": "integer"
  "timeslotduration":
    "type": "integer"
  "timeslotquantity":
    "type": "integer"
  "windowbegin":
    "type": "integer",
  "windowend":
    "type": "integer"
  "required": ["provider", "name"]

exports.navigation =
  "active":
    "type": "integer"
  "admin":
    "type": "integer"
  "caption":
    "type": "string"
  "destination":
    "type": "string"
  "icon":
    "type": "string"
  "label":
    "type": "string"
  "ordering":
    "type": "integer"
  "role":
    "type": "string"
  "titlebar":
    "type": "object"
  "required": ["caption", "destination", "ordering"]

exports.notification =
  "method":
    "type": "string"
  "target":
    "type": "string"
  "trigger":
    "type": "string"
  "interval":
    "type": "integer"
  "required": ["method", "target", "trigger", "interval"]

exports.question =
  "requirement":
    "type": "boolean"
  "placeholder":
    "type": "string"
  "prompt":
    "type": "string"
  "order":
    "type": "integer"
  "options":
    "type": "array"
  "questiontype":
    "type": "string"
  "required": ["questiontype", "order", "prompt", "requirement"]

exports.resource =
  "availability":
    "type": "object"
  "description":
    "type": "string"
  "name":
    "type": "string"
  "provider":
    "type": "string"
  "visible":
    "type": "boolean"
  "required": ["name", "provider"]

exports.user =
  "firstname":
    "type": "string"
  "lastname":
    "type": "string"
  "email":
    "type": "string"
  "password":
    "type": "string"
  "role":
    "type": "string"
  "provider":
    "type": "string"
  "required": ["firstname", "lastname", "email", "password", "role", "provider"]

exports.whitelabel =
  "bookinglogo":
    "type": "string"
  "dictionary":
    "type": "object"
  "label":
    "type": "string"
  "logourl":
    "type": "string"
  "name":
    "type": "string"
  "navigation":
    "type": "array"
  "questions":
    "type": "array"
  "url":
    "type": "string"
  "required": ["name", "url"]
